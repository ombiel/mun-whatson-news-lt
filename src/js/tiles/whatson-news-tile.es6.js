const { LiveTile, registerTile, getLibraries } = require("@ombiel/exlib-livetile-tools");
const screenLink = require('@ombiel/aek-lib/screen-link');
const AekStorage = require("@ombiel/aek-lib/storage");

const momentTimezone = require("moment-timezone");
const moment = require("moment");

let storage = false;
let authStorageKey = false;
let publicStorageKey = false;
let personalStorageKey = false;
let cacheStorageKey = false;

// pull in custom CSS
const newsCSS = require("../../css/news");

// define variables in the root scope of your module
let _;

// when fulfilled, assign them to the variables defined above
// we also assign the returned promise to a variable we can reference later
let libsReady = getLibraries(['lodash', 'jquery']).then((libs) => {
  [_] = libs;
});

class WhatsonNewsTile extends LiveTile {

  onReady() {
    this.throttledRender = _.throttle(this.render, 1000);
    this.showOriginalFace(() => {
      this.setOptions();
      this.init();
    });
  }

  init() {
    let fetchData = true;

    let now = moment();
    if (this.dataCacheDataExpiry) {
      // let fallback = 'Australia/Sydney';
      let timezone = momentTimezone.tz.guess();
      let timeNow = momentTimezone.tz(now, timezone);

      // use the cached expiry to build a timestamp
      let cachedTimestamp = moment(this.dataCacheDataExpiry, "X");
      cachedTimestamp = momentTimezone.tz(cachedTimestamp, timezone);

      // check if we've reached the expiry date and invalidate fetchData if necessary
      if (cachedTimestamp.isAfter(timeNow)) {
        fetchData = false;
      }
    }

    let delay = this.timer(this.initPause, () => { }).promise;
    Promise.all([libsReady, delay]).then(() => {
      if (fetchData) {
        // integrate any delays set by the tile attributes
        this.fetchData(true);
      } else {
        // lastUpdated is only set in cache on a successful service call, so we can guarantee its existing
        let data = storage.get(personalStorageKey);
        if (!data ){
          data = storage.get(publicStorageKey);
        }
        this.cachedUpdate(data);
      }
    });
  }

  translateLocationCode(code) {
    switch (code) {
      case "MURDOCH":
        this.location = "perth";
        break;
      case "ROCKINGHAM":
        this.location = "rockingham";
        break;
      case "MANDURAH":
        this.location = "mandurah";
        break;
      case "DUBAI-ISC":
        this.location = "dubai";
        break;
      case "KAPLAN-SGP":
        this.location = "singapore";
        break;
      case "KAPLAN-MMR":
        this.location = "myanmar";
        break;
      default:
        this.location = false;
    }
  }

  fetchData(firstRun = false) {
    if (!this.dataLoading) {
      // we want the loading in the background after the initial fetch
      this.dataLoading = true;
      // we don't want a throttledRender; we always want to make sure the loading spinner is displayed
      this.render();

      this.fetchCourses();
    }
  }

  fetchCourses() {
    let url = screenLink('mun-whatson-news/courses');
    this.ajax({ url: url })
    .done((res) => {
      // console.warn("courses response ::" );
      // console.log(res);

      if (res && _.has(res, "response.courseList") && res.response.courseList.length > 0) {
        this.location =  res.response.courseList[0].location;
        this.courseLevel = res.response.courseList[0].courseLevel;
      }

      this.fetchStudent();
    });
  }

  fetchStudent() {
    let url = screenLink('mun-whatson-news/student');
    this.ajax({ url: url })
    .done((res) => {
      // console.warn("student response ::" );
      // console.log(res);

      if (res && _.has(res, "response.data")) {
        this.studentType = res.response.data.studentType;
      }

      // tags can be blank, and this is fine for the API call
      let location = this.location ? this.location : "";
      let courseLevel = this.courseLevel ? this.courseLevel : "";
      let studentType = this.studentType ? this.studentType : "";
      this.fetchNews(location, courseLevel, studentType);
    });
  }

  fetchNews(location, courseLevel, studentType) {
    let url = screenLink('mun-whatson-news/data');
    return new Promise((resolve) => {
      this.ajax({ url: url, data: { location:location, courseLevel:courseLevel, studentType:studentType } })
        .done((res) => {
          // console.warn("personalised news :: ");
          // console.log(res);

          if (res && _.has(res, "response.newsList") && res.response.newsList.length > 0) {
            this.update(res, true);
          } else {
            this.fetchPublicNews();
          }
        }).always(() => {
          if (this.fetchTimer) { this.fetchTimer.stop(); }
          // refreshTimer is a common way to control live tile data flow, but Murdoch want to define it based on cache expiry
          // make sure we handle refreshTimer explicitly being disabled, or if we have a cache expiry set instead
          if (this.dataCacheDays === 0 && this.dataCacheHours === 0 && this.refreshTimer > 0) {
            this.fetchTimer = this.timer(this.refreshTimer, this.fetchData.bind(this));
          }

          resolve();
        });
    });
  }

  fetchPublicNews() {
    let url = screenLink('mun-whatson-news/publicData');
    return new Promise((resolve) => {
      this.ajax({ url: url })
        .done((res) => {
          // console.warn("public news :: ");
          // console.log(res);

          if (res && _.has(res, "response.newsList")) {
            if (res.response.newsList.length > 0){
              this.update(res, false);
            } else {
              // empty data
              //this.showOriginalFace();
              this.dataLoading = false;
              this.throttledRender();
            }
          } else {
            this.errorRender();
          }
        }).always(() => {
          if (this.fetchTimer) { this.fetchTimer.stop(); }
          // refreshTimer is a common way to control live tile data flow, but Murdoch want to define it based on cache expiry
          // make sure we handle refreshTimer explicitly being disabled, or if we have a cache expiry set instead
          if (this.dataCacheDays === 0 && this.dataCacheHours === 0 && this.refreshTimer > 0) {
            this.fetchTimer = this.timer(this.refreshTimer, this.fetchData.bind(this));
          }

          resolve();
        });
    });
  }
  

  getCMAttrs() {
    if (storage && authStorageKey) {
      return storage.get(authStorageKey);
    } else {
      return false;
    }
  }

  getCache() {
    if (storage && personalStorageKey) {
      let cache = storage.get(personalStorageKey);
      
      if (cache) {
        return cache;
      } else {
        return storage.get(publicStorageKey);
      }
    } else {
      return false;
    }
  }

  update(res, personal) {
    let data = res.response.newsList;

    this.dataLoading = false;
    if (data && !_.isEqual(data, this.data)) {
      this.data = data;
      this.throttledRender();

      if (personal && storage && personalStorageKey){
        storage.set(personalStorageKey, data);
      } else if (!personal && storage && publicStorageKey) {
        storage.set(publicStorageKey, data);
      }

      if (this.dataCacheDays > 0 || this.dataCacheHours > 0) {
        let now = moment();
        let timezone = momentTimezone.tz.guess();
        let timeNow = momentTimezone.tz(now, timezone);

        if (this.trackHoursAndDays) {
          timeNow.add(this.dataCacheDays, "days");
          timeNow.add(this.dataCacheHours, "hours");
        } else if (this.dataCacheDays > 0) {
          // prioritise days setting over hours, if set 
          timeNow.add(this.dataCacheDays, "days");
        } else if (this.dataCacheHours > 0) {
          timeNow.add(this.dataCacheHours, "hours");
        }

        // console.warn("setting expiry to " + timeNow.format());
        if (storage && cacheStorageKey) {
          storage.set(cacheStorageKey, timeNow.format("X"));
        }
      } else {
        // we can assume both options have deliberately been set to 0 or otherwise disabled; remove the limiter
        if (cacheStorageKey) {
          for (let key in window.localStorage) {
            if (key.indexOf(cacheStorageKey) > -1) {
              window.localStorage.removeItem(key);
              break;
            }
          }
        }
      }
    }

    if (storage && authStorageKey && res.attrs) {
      storage.set(authStorageKey, res.attrs);
    }
  }

  cachedUpdate(data) {
    this.dataLoading = false;
    if (data && !_.isEqual(data, this.data)) {
      this.data = data;
      this.throttledRender();
    }
  }

  errorRender() {
    this.dataLoading = false;

    this.error = this.errorText;
    this.throttledRender();
  }


  render() {
    let content;
    let tileAttributes = this.getTileAttributes();
    let tileWidth = tileAttributes.tileWidth ? tileAttributes.tileWidth : 2;
    if (this.dataLoading) {
      let textClasses = newsCSS.loading;
      if (tileWidth === 2) {
        textClasses += " " + newsCSS.reduced;
      }

      content = `
        <div class="${textClasses}">
          <p>${this.loadingText}</p>
        </div>
      `;

      // this.setFace(content);
    } else if (this.error) {
      // default to the smaller width just to ensure that nomatter what, all content is visible
      // let tileWidth = tileAttributes.tileWidth ? tileAttributes.tileWidth : 2;
      let textClasses = newsCSS.error;
      if (tileWidth === 2) {
        textClasses += " " + newsCSS.reduced;
      }

      content = `
      <div class = "${newsCSS.faceContainer}">
        <div class="${textClasses}"> 
          <p>${this.errorText}</p>
        </div>
      </div>
      `;

      this.flipFace(content);
    } else {
      let data = this.data ? this.data.find((news) => news.rank === 1) : false;
      // data = undefined;
      // console.log(data);

      if (!data) {
        let textClasses = newsCSS.error;
        if (tileWidth === 2) {
          textClasses += " " + newsCSS.reduced;
        }

        content = `
        <div class = "${newsCSS.faceContainer}">
          <div class="${textClasses}"> 
            <p>${this.emptyText}</p>
            </div>
          </div>
        </div>
        `;
      } else {          
        let imageURL = data.thumbnailImageUrl ? data.thumbnailImageUrl : "https://portal-ap.campusm.exlibrisgroup.com/assets/Murdoch/Murdoch/news-fallback-1.png";

        let encodedDataURL = data.displayUrl ? "campusm://openURL?url=" + encodeURIComponent(data.displayUrl) : false;
        let aekToolbarUrl = false;
        if(this.aekMenuReference && this.aekMenuReference.length > 0) {
          aekToolbarUrl = "campusm://loadaek?toolbar=" + this.aekMenuReference;
        }

        content = `
          <div class="${newsCSS.faceContainer}">
            <div class="${newsCSS.whatsOnEvents}">
              <a href="#" data-hotspot=${encodedDataURL} class="${newsCSS.imageHotspot}">
                <div class="${newsCSS.image}" style="background-image: url(${imageURL})"/>
              </a>
              <div class="${newsCSS.rightPane}">
                <a href="#" data-hotspot=${encodedDataURL} class="${newsCSS.textHotspot}">
                  <div class="${newsCSS.article}">
                    <p class="${newsCSS.latestNews}">${this.titleText}</p>
                    <p class="${newsCSS.title}">${data.title}</p>
                    <p class="${newsCSS.date}">${data.date}</p>
                  </div>
                </a>
                <a href="#" data-hotspot="${aekToolbarUrl}" class="${newsCSS.button}">
                  <h2 class="${newsCSS.buttonText}">${this.buttonText}</h2>
                </a>
              </div>
            </div>
          </div>`;
      }
    }

    this.currentFace = this.flipFace(content);
    this.currentFace.onHotspot((url) => {
      // console.log(url);
      if(url) {
        this.openURL(url);
      }
    });
  }


  validatePossibleZeroOption(name, option) {
    let success = false;

    // standard || fallback seems to be triggering for an initPause of 0; we want to be able to set this to 0
    if (typeof option === "number" && option > -1) {
      this[name] = option;
      success = true;
    } else if (option === false) {
      // remember to handle explicitly setting it to false, any exceptions can be set after the fact
      this[name] = 0;
      success = true;
    }

    return success;
  }

  setOptions() {
    let tileAttributes = this.getTileAttributes();
    let whatsonOptions = tileAttributes.whatson || {};

    let storageBaseString = whatsonOptions.storageBaseString || "murdoch_portal";
    storage = new AekStorage(storageBaseString); // prefixes "_aek_store_" to any key, so "_aek_store_murdoch_mydetails"

    // AEKStorage adds "__" between the name of the storage - storageBaseString - and the key value we're actually setting in localStorage
    authStorageKey = whatsonOptions.authStorageKey || "cmAttrs"; // "_aek_store_murdoch_mydetails__cmAttrs"
    personalStorageKey = whatsonOptions.personalStorageKey || "personal_news"; // "_aek_store_murdoch_mydetails__profile"
    publicStorageKey = whatsonOptions.publicStorageKey || "public_news"; // "_aek_store_murdoch_mydetails__profile"

    this.aekMenuReference = whatsonOptions.aekMenuReference && whatsonOptions.aekMenuReference.length > 0 ? whatsonOptions.aekMenuReference : "";

    this.animationPause = parseInt(whatsonOptions.pause) || 3000;
    this.initPause = this.animationPause;
    // standard || fallback seems to be triggering for an initPause of 0; we *want* to be able to set this to 0
    if (typeof whatsonOptions.initPause === "number" && whatsonOptions.initPause > -1) {
      this.initPause = parseInt(whatsonOptions.initPause);
    }

    // validate any unsupported value for this, and default the cache to a single day if necessary
    let dataCacheDaysValidated = this.validatePossibleZeroOption("dataCacheDays", whatsonOptions.dataCacheDays);
    if (!dataCacheDaysValidated) {
      this.dataCacheDays = 1;
    }

    let dataCacheHoursValidated = this.validatePossibleZeroOption("dataCacheHours", whatsonOptions.dataCacheHours);
    if (!dataCacheHoursValidated) {
      this.dataCacheHours = 0;
    }

    // force this to be a boolean
    this.trackHoursAndDays = whatsonOptions.trackHoursAndDays === true ? true : false;

    cacheStorageKey = whatsonOptions.cacheStorageKey || "dataExpiry";
    cacheStorageKey = "news" + "_" + cacheStorageKey;
    this.dataCacheDataExpiry = storage.get(cacheStorageKey);
    if (!this.dataCacheDataExpiry) {
      this.dataCacheDataExpiry = false;
    }

    this.titleText = whatsonOptions.titleText || "LATEST NEWS";
    this.loadingText = whatsonOptions.loadingText || "We’re fetching the latest news...";
    this.errorText = whatsonOptions.errorText || "Oops! We have having technical issues. Please try again later.";
    this.emptyText = whatsonOptions.emptyText || "There is no news to display right now. Please check back later.";
    this.buttonText = whatsonOptions.buttonText || "VIEW ALL NEWS";
    this.refreshTimer = parseInt(whatsonOptions.refreshTimer) || 600000;

    // explicitly declaring values expected in render() to avoid complications with undefined
    this.dataLoading = false;
    this.data = false;
    this.attrs = false;
  }

}

registerTile(WhatsonNewsTile, "whatsOnNews");
